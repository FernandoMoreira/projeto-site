
package br.com.senac.projetosite.dao;


import br.com.senac.projetosite.model.Usuario;
import java.util.List;
import javax.persistence.Query;

public class UsuarioDAO extends DAO<Usuario>{
    
    public UsuarioDAO() {
        super(Usuario.class);
    }
    
    public static void main(String[] args) {
       /* Usuario usuario = new Usuario();
        usuario.setNome("Fernando");
        usuario.setCidade("Cariacica");
        usuario.setEmail("fernando@hotmail.com");
        usuario.setCpf("9999999999");
        usuario.setEndereco("Rua Jose ");
        usuario.setNumero(100);
        usuario.setLogin(master);
        usuario.setSenha(123456);
        
        
        UsuarioDAO dao = new UsuarioDAO(); 
        
        dao.save(usuario);*/
        
    }
    
   public List<Usuario> findByFiltro(String codigo, String nome) {
        this.em = JPAUtil.getEntityManager();
        List<Usuario> lista;
        em.getTransaction().begin();

        StringBuilder sql = new StringBuilder("from Usuario a where 1=1 ");

        if (codigo != null && !codigo.isEmpty()) {
            sql.append(" and a.id = :Id ");
        }

        if (nome != null && !nome.isEmpty()) {
            sql.append(" and a.nome like :Nome ");
        }
        
         Query query = em.createQuery(sql.toString());
        
        if (codigo != null && !codigo.isEmpty()) {
           query.setParameter("Id", new Long(codigo));
        }

        if (nome != null && !nome.isEmpty()) {
            query.setParameter("Nome", "%" + nome + "%");
        }


        lista = query.getResultList();

        em.getTransaction().commit();
        em.close();

        return lista;
    }
}
